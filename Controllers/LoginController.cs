﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeadNoteNew.DTOs;
using DeadNoteNew.Interfaces;
using DeadNoteNew.Databases;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using DeadNoteNew.Services;
using MimeKit;
using DeadNoteNew.Models;
//using DeadNote01.Models;

namespace DeadNoteNew.Controllers
{
    public class LoginController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<AppUser> _userManager;
        private readonly ISendMailService _sendMailService;

        /// <summary>
        /// Initialize login controller
        /// </summary>
        /// <param name="db"></param>
        /// <param name="userManager"></param>
        /// <param name="sendMailService"></param>
        public LoginController(ApplicationDbContext db, UserManager<AppUser> userManager, ISendMailService sendMailService)
        {
            _sendMailService = sendMailService;
            _userManager = userManager;
            _db = db;
        }

        /// <summary>
        /// Index view for this controller
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            IEnumerable<Account> accountList = this._db.Accounts;
            return View(accountList);
        }

        /// <summary>
        /// Show login view
        /// </summary>
        /// <returns></returns>
        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }


        /// <summary>
        /// Callback function to login, check if model valid and validate token before submit
        /// </summary>
        /// <param name="loginDto"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult<AccountDto>> Login(LoginDto loginDto)
        {
            if (ModelState.IsValid)
            {

                //xem them ve singleordefaultasync
                var account = await _db.Accounts.SingleOrDefaultAsync(x => x.Email == loginDto.Email);

                if (account == null) return Unauthorized("Invalid email or password");

                if (account.ConfirmEmail == false) return RedirectToAction("Error");

                var computeHash = GetMD5Hash(loginDto.Password);

                for (int i = 0; i < computeHash.Length; i++)
                {
                    if (computeHash[i] != account.Password[i])
                    {
                        return View();
                    }
                }

                //set the key value in Cookie  
                Set("uid", account.Id.ToString(), 10);

                return RedirectToAction("Index");
            }
            return View(loginDto);

        }

        /// <summary>
        /// Logout user from system
        /// </summary>
        /// <returns></returns>
        public IActionResult Logout()
        {
            //Delete the cookie object  
            Remove("uid");

            return RedirectToAction("Login");
        }

        public string Get(string key)
        {
            return Request.Cookies[key];
        }

        public void Set(string key, string value, int? expireTime)
        {
            CookieOptions option = new CookieOptions();
            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddMinutes(expireTime.Value);
            else
                option.Expires = DateTime.Now.AddMilliseconds(10);
            Response.Cookies.Append(key, value, option);
        }

        public void Remove(string key)
        {
            Response.Cookies.Delete(key);
        }

        public static string GetMD5Hash(string str)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] bytes = ASCIIEncoding.Default.GetBytes(str);
            byte[] encoded = md5.ComputeHash(bytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < encoded.Length; i++)
                sb.Append(encoded[i].ToString("x2"));

            return sb.ToString();
        }

        public IActionResult SendPasswordResetLink()
        {
            // TODO: Your code here
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult SendPasswordResetLink(string email)
        {
            AppUser user = _userManager.FindByEmailAsync(email).Result;

            if (user == null || !(user.EmailConfirmed))
            {
                ViewBag.Message = "Error while resetting your password!";
                return View();
            }

            var account = this._db.Accounts.SingleOrDefault(x => x.Email == email);
            account.resetTokenStatus = false;
            _db.SaveChanges();

            var token = _userManager.GeneratePasswordResetTokenAsync(user).Result;

            var resetLink = Url.Action("ResetPassword", "Login",
            new
            {
                email = user.Email,
                token = token
            }, protocol: HttpContext.Request.Scheme);

            InternetAddressList list = new InternetAddressList();
            // list.Add(new MailboxAddress(email));

            // code to email the above link
            var mailContent = new MailContent();
            mailContent.To = "baolhqce150509@fpt.edu.vn";
            mailContent.Subject = "RESET PASSWORD LINK!";
            mailContent.Body = resetLink;
            var rs = _sendMailService.SendMail(mailContent, list);

            return View("Login");

        }

        //Reset Password Field
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string token, string email)
        {
            // If password reset token or email is null, most likely the
            // user tried to tamper the password reset link
            var account = this._db.Accounts.SingleOrDefault(c => c.Email == email);

            if (account.resetTokenStatus == true)
            {
                ViewBag.ResetTokenError = "Token da duoc su dung roi!";
                ViewBag.IsUsed = true;
            }
            else
            {
                ViewBag.IsUsed = false;
            }
            if (token == null || email == null)
            {
                ModelState.AddModelError("", "Invalid password reset token");
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Find the user by email
                var user = await _userManager.FindByEmailAsync(model.Email);
                var account = this._db.Accounts.SingleOrDefault(c => c.Email == model.Email);
                if (user != null)
                {

                    // reset the user password
                    var result = await _userManager.ResetPasswordAsync(user, model.Token, model.Password);
                    if (result.Succeeded)
                    {
                        account.Password = GetMD5Hash(model.Password);
                        account.resetTokenStatus = true;
                        this._db.SaveChanges();
                        return View("ResetPasswordConfirmation");
                    }
                    // Display validation errors. For example, password reset token already
                    // used to change the password or password complexity rules not met
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                    return View(model);
                }

                // To avoid account enumeration and brute force attacks, don't
                // reveal that the user does not exist
                return View("Error");
            }
            // Display validation errors if model state is not valid
            return View(model);
        }



    }
}