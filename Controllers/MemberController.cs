﻿using System;
using System.Linq;
using DeadNoteNew.Databases;
using Microsoft.AspNetCore.Mvc;

namespace DeadNoteNew.Controllers
{
    public class MemberController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MemberController(ApplicationDbContext context)
        {
            _context = context;
        }
        
        public IActionResult Index(Guid id)
        {
            var uid = Request.Cookies["uid"];
            if (!Utils.CheckGuid(uid)) return Redirect("/Login");

            var guid = Guid.Parse(uid);
            var account = _context.Accounts.Find(guid);
            if (account == null) return Redirect("/Login");

            var foundClass = _context.Classes.Find(id);
            if (foundClass == null) return Redirect("/Class");

            var members = _context.Accounts.Where(a => a.Classes.Any(c => c.Id == id));

            ViewData["foundClass"] = foundClass;
            ViewData["members"] = members;
            
            return View();
        }
    }
}