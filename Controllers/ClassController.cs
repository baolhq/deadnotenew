﻿using System;
using System.Linq;
using DeadNoteNew.Databases;
using DeadNoteNew.Models;
using Microsoft.AspNetCore.Mvc;

namespace DeadNoteNew.Controllers
{
    public class ClassController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ClassController(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get all class which user take part in
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            var uid = Request.Cookies["uid"];

            if (!Utils.CheckGuid(uid)) return Redirect("/Login");

            var guid = Guid.Parse(uid);
            var account = _context.Accounts.Find(guid);

            if (account == null) return Redirect("/Login");

            var accountClasses = _context.Classes.Where(c => c.Accounts.Any(a => a.Id == guid));

            ViewData["account"] = account;

            return View(accountClasses);
        }

        /// <summary>
        /// Delete class with specific id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult Delete(string id)
        {
            if (string.IsNullOrEmpty(id)) return NotFound();

            var guid = Guid.Parse(id);

            var delClass = _context.Classes.Find(guid);
            if (delClass == null) return NotFound();

            var members = _context.Accounts.Where(a => a.Classes.Any(c => c.Id == guid));

            ViewData["delClass"] = delClass;
            ViewData["members"] = members;

            return View();
        }

        /// <summary>
        /// Callback function after user submit delete form, check if user exists and perform delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult DeleteConfirmed(string id)
        {
            if (string.IsNullOrEmpty(id)) return NotFound();

            var guid = Guid.Parse(id);

            var delClass = _context.Classes.Find(guid);
            if (delClass == null) return NotFound();

            delClass.Status = false;
            _context.Classes.Update(delClass);
            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Get form to create new class, only teacher can create class
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            var uid = Request.Cookies["uid"];
            if (!Utils.CheckGuid(uid)) return Redirect("/Login");

            var guid = Guid.Parse(uid);
            var account = _context.Accounts.Find(guid);
            if (account == null) return Redirect("/Register");

            // If user is not teacher redirect to /Class
            if (!account.IsTeacher) return RedirectToAction("Index");
            return View();
        }

        /// <summary>
        /// Callback function for create class, save class to database
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="studentGroup"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Create(string subject, string studentGroup)
        {
            var uid = Request.Cookies["uid"];
            if (!Utils.CheckGuid(uid)) return Redirect("/Login");

            var guid = Guid.Parse(uid);
            var account = _context.Accounts.Find(guid);
            if (account == null) return Redirect("/Login");

            // If user is not teacher redirect to /Class
            if (!account.IsTeacher) return RedirectToAction("Index");

            // Create class name with subject and student group
            if (string.IsNullOrEmpty(subject) || string.IsNullOrEmpty(studentGroup)) return BadRequest();
            var className = $"{subject}_{studentGroup}";

            // Class name exist
            if (_context.Classes.Any(c => c.Name == className))
            {
                ViewData["classNameExist"] = $"{className} already exist.";
                return View("Create");
            }

            var newClass = new Class { Id = Guid.NewGuid(), Name = className, Status = true };
            newClass.Accounts.Add(account);

            _context.Classes.Add(newClass);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Leave class, remove the link between student and that class
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult Leave(Guid id)
        {
            var uid = Request.Cookies["uid"];

            if (!Utils.CheckGuid(uid)) return Redirect("/Login");

            var guid = Guid.Parse(uid);
            var account = _context.Accounts.Find(guid);
            if (account == null) return Redirect("/Login");

            var leavingClass = _context.Classes.Find(id);
            if (leavingClass == null) return NotFound();

            var members = _context.Accounts.Where(a => a.Classes.Any(c => c.Id == id));

            ViewData["leavingClass"] = leavingClass;
            ViewData["members"] = members;

            return View();
        }
    }
}