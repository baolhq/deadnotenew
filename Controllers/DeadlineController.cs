﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DeadNoteNew.Databases;
using DeadNoteNew.Interfaces;
using DeadNoteNew.Models;
using DeadNoteNew.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MimeKit;

namespace DeadNoteNew.Controllers
{
    public class DeadlineController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ISendMailService _sendMailService;

        /// <summary>
        /// Initialize controller
        /// </summary>
        /// <param name="context"></param>
        /// <param name="sendMailService"></param>
        public DeadlineController(ApplicationDbContext context, ISendMailService sendMailService)
        {
            _context = context;
            _sendMailService = sendMailService;
        }

        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {

            // var uid = Request.Cookies["uid"];

            // if (string.IsNullOrEmpty(uid))
            // {
            //     return Redirect("/Login");
            // }

            // var guid = Guid.Parse(uid);
            // var account = _context.Accounts.Find(guid);

            // if (account == null) return Redirect("/Login");

            // var accountClasses = account.Classes;
            // var result = accountClasses.Select(aClass => aClass.Deadlines).ToList();
            // return View(result);
            ViewData["CurrentSort"] = sortOrder;
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            IEnumerable<Deadline> DeadlineList = this._context.Deadlines;
            var obj = _context.Deadlines.Where(s => s.Status == true);
            int pageSize = 3;
            return View(await PaginatedList<Deadline>.CreateAsync(obj.AsNoTracking(), pageNumber ?? 1, pageSize));

            // return View(obj);
        }

        /// <summary>
        /// Find all classes base on input id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult Class(string id)
        {
            if (string.IsNullOrEmpty(id)) return NotFound();

            var uid = Request.Cookies["uid"];
            if (!Utils.CheckGuid(uid)) return Redirect("/Login");

            var foundClass = _context.Classes.Find(Guid.Parse(id));
            ViewData["className"] = foundClass.Name;

            var result = _context.Classes.Where(c => c.Id == Guid.Parse(id)).SelectMany(c => c.Deadlines);

            return View(result);
        }

        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Create new deadline, only teacher can access this function
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="StartTime"></param>
        /// <param name="StartDate"></param>
        /// <param name="EndTime"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Obsolete]
        public IActionResult Create(Deadline obj, string StartTime, DateTime StartDate, string EndTime, DateTime EndDate)
        {
            if (ModelState.IsValid)
            {
                obj.Status = true;
                DateTime s = StartDate;
                var time = StartTime.Split(":");
                TimeSpan ts = new TimeSpan(int.Parse(time[0]), int.Parse(time[1]), 0);
                s = s.Date + ts;
                obj.StartDate = s;

                DateTime s1 = EndDate;
                var time1 = EndTime.Split(":");
                TimeSpan ts1 = new TimeSpan(int.Parse(time1[0]), int.Parse(time1[1]), 0);
                s1 = s1.Date + ts1;
                obj.EndDate = s1;

                var builder = new BodyBuilder();
                //Giao thuc IO Truyen file
                using (StreamReader SourceReader = System.IO.File.OpenText(@"C:\Users\quocb\RiderProjects\DeadNoteNew\Controllers\Templates\index.html"))
                {
                    builder.HtmlBody = SourceReader.ReadToEnd();
                }
                // replace chữ trong index
                string htmlBody = builder.HtmlBody.Replace("PARTY", obj.Title)
                .Replace("date1", obj.StartDate.ToString())
                .Replace("date2", obj.EndDate.ToString())
                .Replace("Placeholder1", obj.Content)
                .Replace("Placeholder2", "\"Your time is limited, so don’t waste it living someone else’s life\" <br> - <i>Steve Jobs</i>");
                string messagebody = string.Format("{0}", htmlBody);

                //List ra tat ca mail hien co trong table Account
                InternetAddressList list = new InternetAddressList();
                var emailList = this._context.Accounts;
                foreach (var item in emailList)
                {
                    list.Add(new MailboxAddress(item.Email));
                }

                var mailContent = new MailContent();
                // mailContent.To = "LongHBCE150048@fpt.edu.vn";
                mailContent.Subject = "DEADNOTE THÔNG BÁO";
                mailContent.Body = messagebody;
                var rs = _sendMailService.SendMail(mailContent, list);

                TempData["StatusMessage"] = "DeadLine has been added ";
                this._context.Deadlines.Add(obj);
                this._context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(obj);
        }

        /// <summary>
        /// Get deadline info
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult Update(Guid id)
        {
            var obj = this._context.Deadlines.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
            ViewData["obj"] = obj;
            return View(obj);
        }

        /// <summary>
        /// Update deadline information
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Title"></param>
        /// <param name="Content"></param>
        /// <param name="StartTime"></param>
        /// <param name="StartDate"></param>
        /// <param name="EndTime"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(Guid id, string Title, string Content, string StartTime, DateTime StartDate, string EndTime, DateTime EndDate)
        {
            var obj = this._context.Deadlines.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
            if (Title == null || Content == null)
            {
                return View(obj);
            }
            obj.Title = Title;
            obj.Content = Content;
            DateTime s = StartDate;
            var time = StartTime.Split(":");
            TimeSpan ts = new TimeSpan(int.Parse(time[0]), int.Parse(time[1]), 0);
            s = s.Date + ts;
            obj.StartDate = s;

            DateTime s1 = EndDate;
            var time1 = EndTime.Split(":");
            TimeSpan ts1 = new TimeSpan(int.Parse(time1[0]), int.Parse(time1[1]), 0);
            s1 = s1.Date + ts1;
            obj.EndDate = s1;
            TempData["StatusMessage"] = "DeadLine has been updated ";
            this._context.Deadlines.Update(obj);
            this._context.SaveChanges();
            return RedirectToAction("Index");
        }


        /// <summary>
        /// Get deadline info
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult Delete(Guid id)
        {
            var obj = this._context.Deadlines.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
            return View(obj);
        }

        /// <summary>
        /// Delete deadline with specific id, only teacher can access
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(Guid? Id)
        {
            var item = _context.Deadlines.Find(Id);
            // var obj = this._db.Categories.Find(cat_id);
            if (item == null)
            {
                return NotFound();
            }
            item.Status = false;
            IEnumerable<Deadline> deadlines1 = this._context.Deadlines;
            var deadlines = deadlines1.Select(p => p).Where(p => p.Id == Id);
            foreach (var obj in deadlines)
            {
                obj.Status = false;
            }
            TempData["StatusMessage"] = "DeadLine has been delete ";
            this._context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}