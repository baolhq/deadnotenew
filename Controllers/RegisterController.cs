﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeadNoteNew.Databases;
using DeadNoteNew.DTOs;
using DeadNoteNew.Interfaces;
using DeadNoteNew.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System;
using DeadNoteNew.Services;

namespace DeadNoteNew.Controllers
{
    public class RegisterController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<AppUser> _userManager;
        private readonly ISendMailService _sendMailService;
        public RegisterController(ApplicationDbContext db, UserManager<AppUser> userManager, ISendMailService sendMailService)
        {
            _sendMailService = sendMailService;
            _userManager = userManager;
            _db = db;
        }

        public IActionResult Index()
        {
            IEnumerable<Account> list = this._db.Accounts;
            return View(list);
        }

        public IActionResult Register()
        {
            // TODO: Your code here
            return View();
        }

        [HttpPost]
        public IActionResult Register(RegisterDto registerDto)
        {
            // TODO: Your code here
            if (ModelState.IsValid)
            {
                var check = this._db.Accounts.SingleOrDefault(c => c.Email == registerDto.Email);
                if (check != null)
                {
                    return View();
                }
                for (int i = 0; i < registerDto.Password.Length; i++)
                {
                    if (registerDto.Password[i] != registerDto.ConfirmPassword[i])
                    {
                        ModelState.AddModelError("", "Confirm Password must match");
                        return View();
                    }
                }

                AppUser user = new AppUser()
                {
                    UserName = registerDto.FullName,
                    Email = registerDto.Email
                };

                IdentityResult result = _userManager.CreateAsync(user, registerDto.Password).Result;
                if (result.Succeeded)
                {
                    string confirmationToken = _userManager.GenerateEmailConfirmationTokenAsync(user).Result;
                    string confirmationLink = Url.Action("ConfirmEmail", "Register",
                    new
                    {
                        email = user.Email,
                        token = confirmationToken
                    }, protocol: HttpContext.Request.Scheme);

                    var list = new InternetAddressList();
                    // var emailList = this._db.Accounts;
                    // foreach (var obj in emailList)
                    // {   
                    //     list.Add(new MailboxAddress(obj.Email));
                    // }
                    // list.Add(new MailboxAddress(registerDto.Email));
                    var mailContent = new MailContent();
                    mailContent.To = "baolhqce150509@fpt.edu.vn";
                    mailContent.Subject = "KIEM TRA THU EMAIL";
                    mailContent.Body = confirmationLink;
                    var rs = _sendMailService.SendMail(mailContent, list);

                    var account = new Account
                    {
                        Id = registerDto.Id,
                        Email = registerDto.Email,
                        Password = GetMD5Hash(registerDto.Password),
                        FullName = registerDto.FullName,
                        IsTeacher = registerDto.IsTeacher,
                        IsAdmin = registerDto.IsAdmin,
                        Gender = registerDto.Gender,
                        Birthday = registerDto.Birthday,
                        Address = registerDto.Address
                    };

                    this._db.Accounts.Add(account);
                    this._db.SaveChanges();

                    return View("Error");
                }
                return View();
            }
            return View();

        }

        public static string GetMD5Hash(string str)
        {
            var md5 = new MD5CryptoServiceProvider();
            byte[] bytes = ASCIIEncoding.Default.GetBytes(str);
            byte[] encoded = md5.ComputeHash(bytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < encoded.Length; i++)
                sb.Append(encoded[i].ToString("x2"));

            return sb.ToString();
        }

        public async Task<IActionResult> ConfirmEmail(string email, string token)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(email);
                if (user == null)
                    return View("NotConfirm");

                var result = await _userManager.ConfirmEmailAsync(user, token);

                if (result.Succeeded)
                {
                    var account = _db.Accounts.SingleOrDefault(c => c.Email == email);
                    account.ConfirmEmail = true;
                    _db.SaveChanges();
                }

                return View(result.Succeeded ? "ConfirmEmail" : "NotConfirm");
            }
            catch (InvalidOperationException e)
            {
                return View(e);
            }
        }
        public IActionResult Error()
        {
            // TODO: Your code here
            return View();
        }
    }
}