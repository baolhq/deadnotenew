using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DeadNoteNew.Models
{
    public class ResetPasswordViewModel
    {
        [Key]
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password",
            ErrorMessage = "Password and Confirm Password must match")]
        public string ConfirmPassword { get; set; }

        public string Token { get; set; }

        [DefaultValue(false)]
        public bool resetToken { get; set; }
    }
}