using System.Threading.Tasks;
using DeadNoteNew.Services;
using MimeKit;

namespace DeadNoteNew.Interfaces
{
    public interface ISendMailService
    {
        Task<string> SendMail(MailContent mailContent, InternetAddressList list);

        Task SendEmailAsync(string email, string subject, string htmlMessage);
    }
}