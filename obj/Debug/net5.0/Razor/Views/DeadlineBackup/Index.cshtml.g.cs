#pragma checksum "C:\Users\quocb\RiderProjects\DeadNoteNew\Views\DeadlineBackup\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0b43857ffda72278c27664c560a9d7912590b63c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_DeadlineBackup_Index), @"mvc.1.0.view", @"/Views/DeadlineBackup/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\quocb\RiderProjects\DeadNoteNew\Views\_ViewImports.cshtml"
using DeadNoteNew;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\quocb\RiderProjects\DeadNoteNew\Views\_ViewImports.cshtml"
using DeadNoteNew.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0b43857ffda72278c27664c560a9d7912590b63c", @"/Views/DeadlineBackup/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"89b6f681b6da80d21eebad2c0870bf5993cbfe90", @"/Views/_ViewImports.cshtml")]
    public class Views_DeadlineBackup_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IQueryable<Deadline>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\quocb\RiderProjects\DeadNoteNew\Views\DeadlineBackup\Index.cshtml"
  
    ViewData["Title"] = "Deadline";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<table class=""table table-bordered table-hover table-striped"">
    <caption class=""text-center"">All your deadlines</caption>
    <thead class=""thead-dark"">
    <th>ID</th>
    <th>Title</th>
    <th>Content</th>
    <th>StartDate</th>
    <th>EndDate</th>
    </thead>

    <tbody>
");
#nullable restore
#line 18 "C:\Users\quocb\RiderProjects\DeadNoteNew\Views\DeadlineBackup\Index.cshtml"
         foreach (var item in Model)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\r\n                <td>");
#nullable restore
#line 21 "C:\Users\quocb\RiderProjects\DeadNoteNew\Views\DeadlineBackup\Index.cshtml"
               Write(item.Id);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 22 "C:\Users\quocb\RiderProjects\DeadNoteNew\Views\DeadlineBackup\Index.cshtml"
               Write(item.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 23 "C:\Users\quocb\RiderProjects\DeadNoteNew\Views\DeadlineBackup\Index.cshtml"
               Write(item.Content);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 24 "C:\Users\quocb\RiderProjects\DeadNoteNew\Views\DeadlineBackup\Index.cshtml"
               Write(item.StartDate);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 25 "C:\Users\quocb\RiderProjects\DeadNoteNew\Views\DeadlineBackup\Index.cshtml"
               Write(item.EndDate);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n            </tr>\r\n");
#nullable restore
#line 27 "C:\Users\quocb\RiderProjects\DeadNoteNew\Views\DeadlineBackup\Index.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </tbody>\r\n</table>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IQueryable<Deadline>> Html { get; private set; }
    }
}
#pragma warning restore 1591
