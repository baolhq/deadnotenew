﻿using System;

namespace DeadNoteNew
{
    public static class Utils
    {
        /// <summary>
        /// Check if Guid is valid or not, also trying to parse it to check
        /// </summary>
        /// <param name="id">Guid in string type</param>
        /// <returns>Whether id is a valid Guid</returns>
        public static bool CheckGuid(string id)
        {
            if (string.IsNullOrEmpty(id)) return false;
            return Guid.TryParse(id, out _);
        }
    }
}